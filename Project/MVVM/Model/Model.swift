//
//  ModelFile.swift
//  MVVM+Demo11
//
//  Created by Sudar vizhi on 11/06/21.
//

import Foundation


struct UserModel : Codable {
    var success : String?
    var view_call_details : [View_call_details]?

}
struct DetailModel : Codable {
    var id : Int?
    var name : String?
    var picture : String?
}

struct View_call_details : Codable {
    var id : Int?
    var caller_id : Int?
    var receiver_id : Int?
    var room_name : String?
    var call_status : String?
    var duration : String?
    var type : String?
    var call_type : String?
    var to_detail : DetailModel?
    var is_group : Int?
    var detail_id : Int?
    var created_at : String?

}





