//
//  UserViewModel.swift
//  Project
//
//  Created by Saravana on 07/06/21.
//

import Foundation
import UIKit

// #MARK:- Intiating View Model
class  IntialVM {
    var onGetData:((UserModel)->Void)?
    var vc : UIViewController!
    init(){}
    
    init(vc : UIViewController) {
        self.vc = vc
    }
}

// #MARK:- ViewModel Extension for Home View Controller
extension IntialVM{
    func getModelResponse(){
            let resource = Resource<UserModel>(url: url, method: .get ,params : [:])
            WebService.shared.loadData(resource: resource, complition: {  (result , statusCode)  in
                switch result{
                    case .success(let data) :
                        if statusCode.isResponseOK() {
                            self.onGetData?(data)
                        }else{
                            showToast(msg: "Oops! Something went Wrong")
                        }
                        break
                    case .failure(let error) :
                        Log.er(url : url,error.localizedDescription)
                        break
                }
            })
        }
}






