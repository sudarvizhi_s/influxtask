//
//  MyCollectionViewCell.swift
//  Project
//
//  Created by Sudar vizhi on 11/06/21.
//

import UIKit

class MyCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var myImage: ImageLoader!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initialLoads()
    }
    
    private func initialLoads(){
        contentView.layer.cornerRadius = 5
        contentView.layer.borderWidth = 2
        contentView.layer.borderColor = UIColor.blue.cgColor
    }
    
}

class tutleMyCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        initialLoads()
    }
    private func initialLoads(){
        titleLabel.backgroundColor = .blue
        titleLabel.textColor = .white
    }
}
