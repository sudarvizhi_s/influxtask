//
//  HomeController.swift
//  Project
//
//  Created by Sudar vizhi on 12/06/21.
//

import UIKit

class HomeController: UIViewController {
    @IBOutlet var myCollectionView: UICollectionView!
    
    // variable declarations
    var intialVM : IntialVM?
    var intialModel : UserModel?
    var isSelected = false
    var selectedTxt = ""
    var selectIndex = 0
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.intialLoads()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setNavigationbar()
    }
}
extension HomeController {
    
    private func intialLoads(){
        self.intialVM = IntialVM(vc: self)
        setTableFunctionalities()
        self.intialVM?.getModelResponse()
        self.intialVM?.onGetData = { response in
            self.intialModel = response
            self.myCollectionView.reloadData()
        }
    }
    
    // #MARK:- Intiating TableView Functionalities
    private func setTableFunctionalities(){
        myCollectionView.delegate = self
        myCollectionView.dataSource = self
    }
    
    private func setNavigationbar(){
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        let backBTN = UIBarButtonItem(image: UIImage(named: "back4"), style: .done,
                                      target: navigationController,
                                      action: #selector(UINavigationController.popViewController(animated:)))
        navigationItem.leftBarButtonItem = backBTN
        self.navigationController?.navigationBar.tintColor = UIColor.black
    }
}
     

extension HomeController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if isSelected {
            return (intialModel?.view_call_details?.count ?? 0) + 1
        }
        else {
        return intialModel?.view_call_details?.count ?? 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if isSelected {
            if indexPath.item == (addCell(num: selectIndex) == true ?  (selectIndex + 2) : (selectIndex + 1)) {
                let cell = myCollectionView.dequeueReusableCell(withReuseIdentifier: "tutleMyCollectionViewCell", for: indexPath) as! tutleMyCollectionViewCell
                cell.titleLabel.text = selectedTxt
                return cell
            }else{
                var index = indexPath.row
                if selectIndex < indexPath.row{
                    index = index - 1
                }
                let cell = myCollectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! MyCollectionViewCell
                cell.myImage.loadImageWithUrl(URL(string: intialModel?.view_call_details?[index].to_detail?.picture ?? "")!)
                return cell
            }
        }else{
            let cell = myCollectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! MyCollectionViewCell
            cell.myImage.loadImageWithUrl(URL(string: intialModel?.view_call_details?[indexPath.row].to_detail?.picture ?? "")!)
            return cell
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if isSelected {
            if indexPath.item == (addCell(num: selectIndex) == true ?  (selectIndex + 2) : (selectIndex + 1)) {
                let height = requiredHeight(text:selectedTxt , cellWidth : collectionView.frame.size.width)
                return CGSize(width: collectionView.frame.size.width, height: height + 40)
            }else{
                let size = (collectionView.frame.size.width-20)/2
                return CGSize(width: size, height: size)
            }
        }else{
            let size = (collectionView.frame.size.width-20)/2
            return CGSize(width: size, height: size)
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        var index = indexPath.row
        if isSelected {
            if selectIndex <= indexPath.row {
                index = index - 1
            }
        }
        
        
        if isSelected && (selectIndex == indexPath.row) {
//            isSelected = false
        }else{
            selectIndex = index
            //            let txt = userDataViewModel.nameActions(indexPath: indexPath.row)
            
            selectedTxt = "shgsgfsgf shgfjsgfsfsf shgsgfsgf shgfjsgfsfsf shgsgfsgf shgfjsgfsfsf shgsgfsgf shgfjsgfsfsf shgsgfsgf shgfjsgfsfsf shgsgfsgf shgfjsgfsfsf shgsgfsgf shgfjsgfsfsf shgsgfsgf shgfjsgfsfsf shgsgfsgf shgfjsgfsfsf shgsgfsgf shgfjsgfsfsf shgsgfsgf shgfjsgfsfsf shgsgfsgf shgfjsgfsfsf shgsgfsgf shgfjsgfsfsf shgsgfsgf shgfjsgfsfsf shgsgfsgf shgfjsgfsfsf shgsgfsgf shgfjsgfsfsf"
            isSelected = true
        }
        myCollectionView.reloadData()
    }
    
    
    
}

// Calculating the heights
extension HomeController {
    func requiredHeight(text:String , cellWidth : CGFloat) -> CGFloat {
        let font = UIFont(name: "Helvetica", size: 16.0)
        let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: cellWidth, height: .greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.font = font
        label.text = text
        label.sizeToFit()
        return label.frame.height
        
    }
    
    func addCell(num:Int)->Bool{
        if num.isMultiple(of: 2){
            return true
        }else{
            return false
        }
    }
    
}

